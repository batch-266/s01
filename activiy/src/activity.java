import java.util.Scanner;

public class activity {
    public static void main(String[] args) {
        Scanner firstName = new Scanner(System.in);
        System.out.println("First Name: ");
        String fName = firstName.nextLine();
        Scanner lastName = new Scanner(System.in);
        System.out.println("Last Name: ");
        String lName = lastName.nextLine();
        Scanner grade1 = new Scanner(System.in);
        System.out.println("First Subject Grade: ");
        double grd1 = Double.valueOf(grade1.nextLine());
        Scanner grade2 = new Scanner(System.in);
        System.out.println("Second Subject Grade: ");
        double grd2 = Double.valueOf(grade2.nextLine());
        Scanner grade3 = new Scanner(System.in);
        System.out.println("Third Subject Grade: ");
        double grd3 = Double.valueOf(grade3.nextLine());

        double average = (grd1 + grd2 + grd3) / 3;

        System.out.println("Good day, " + fName + " " + lName);
        System.out.println("Your grade average is: " + average);



    }
}
